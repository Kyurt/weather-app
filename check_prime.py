#!/usr/bin/env python3

import math

def check_prime(number):
    squared_number = math.sqrt(number)
    for i in range(2, int(squared_number) + 1 ):
        if (number / i).is_integer():
            return False
    return True 

print(f'check_prime(10,000,000) = {check_prime(10_000_019)}')
