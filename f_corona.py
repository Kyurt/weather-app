#! /usr/bin/env python3
import requests
from flask import Flask
app = Flask(__name__)

@app.route('/')
def daily_covid19():
    r = requests.get('https://api.covidtracking.com/v1/states/daily.json')
    data = r.json()
    h_dict = {}
    for i in range(500):
        if data[i]['state'] == 'KS':
            h_dict[data[i]['date']] =  data[i]['inIcuCurrently']

    return h_dict
