#! /usr/bin/env python3
import requests


DARK_SKY_SECRET_KEY="1d8c58ed1d54f96f939e706c788650f1"
LOCATION_ENDPOINT = 'https://ipvigilante.com/

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude

    """
    rl = requests.get('LOCATION_ENDPOINT')
    location = rl.json()

    longitude = (location['data']['longitude'])
    latitude = (location['data']['latitude'])
    city = location['data']['city_name']

    return longitude, latitude, city
    print(longitude, latitude)

def get_temperature(longitude, latitude):
    """ Returns the current temperature at the specified location

    Parameters:
    longitude (str): 
    latitude (str):

    Returns:
    float: temperature
    """
    r = requests.get('https://api.darksky.net/forecast/{key}/{lat},{long}'.format(
                    key=DARK_SKY_SECRET_KEY, lat=latitude, long=longitude))

    forecast = r.json()
    time = forecast['currently']['time']
    temp = forecast['currently']['temperature']
    high = forecast['daily']['data'][0]['temperatureHigh']
    low = forecast['daily']['data'][0]['temperatureLow']
    summary = forecast['daily']['data'][0]['summary']
    humidity = forecast['daily']['data'][0]['humidity']
    feels_like = forecast['currently']['apparentTemperature']
    #print(time, temp)
    #today = forecast['daily']['data'][0]
    #print('Today - High: {high}, Low: {low}'.format(high=today['temperatureHigh'], low=today['temperatureLow']))

    return temp, high, low, summary, humidity, feels_like

def print_forecast(temp):
    """ Prints the weather forecast given the specified temperature.
    Parameters:
    temp (float)
    """

    #print(f"Today's forecast is: {temp} degrees!")
    print(f'The weather forecast in {city}\n')
    print(f'{summary}\n')
    print(f'Temprerature: {temp}')
    print(f'High:  {high:>12}')
    print(f'Low: {low:>14}')
    print(f'Humidity:     {humidity*100:.1f}%')
    print(f'Feels Like: {feels_like:7}')

if __name__ == "__main__":
    longitude, latitude, city = get_location()
    temp, high, low, summary, humidity, feels_like = get_temperature(longitude, latitude)
    print_forecast(temp)    
