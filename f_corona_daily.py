#! /usr/bin/env python3
import requests

from flask import Flask
app = Flask(__name__)


@app.route('/')
def data_by_state():
    """ returns daily data by state for 1k datapoints
    Returns
    dictinary days is the key and values is a list [death, ICU, hospitalized]
    """
    r = requests.get('https://api.covidtracking.com/v1/states/daily.json')
    daily_data = r.json()

    us_state = 'KS'
    rows_dict = {}
    for i in range(1000):
        if daily_data[i]['state'] == us_state:
            day = daily_data[i]['date']             
            value = []                  
            value.append(daily_data[i]['death'])
            value.append(daily_data[i]['inIcuCurrently']) 
            value.append(daily_data[i]['hospitalizedCurrently'])
            value.append(daily_data[i]['onVentilatorCurrently'])
            rows_dict[day] = value
    
    return rows_dict
