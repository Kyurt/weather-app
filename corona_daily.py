#! /usr/bin/env python3
import requests
import argparse
from pprint import pprint as pp
import matplotlib.pyplot as pyplot

# Parser will figure out all the arguments
parser = argparse.ArgumentParser(description='Daily Covid19 info for a state') 
# Parser will except positional argument for a state
parser.add_argument('-s', '--state', help='Two character state code, eg :KS', default='KS')
# Parse any argument to the program
args = parser.parse_args() 

def get_daily_data():
    """ Gets daily data per state
    Returns
    daily_data
    """
    r = requests.get('https://api.covidtracking.com/v1/states/daily.json')
    daily_data = r.json()
    
    return daily_data

def data_by_state(us_state='KS'):
    """ returns daily data by state for 1k datapoints
    Returns
    dictinary days is the key and values is a list [death, ICU, hospitalized]
    """
    us_state = args.state.upper()
    rows_dict = {}
    for i in range(1000):
        if daily_data[i]['state'] == us_state:
            day = daily_data[i]['date'] 
            
            value = []                  
            value.append(daily_data[i]['death'])
            value.append(daily_data[i]['inIcuCurrently']) 
            value.append(daily_data[i]['hospitalizedCurrently'])
            value.append(daily_data[i]['onVentilatorCurrently'])
            rows_dict[day] = value
    
    return rows_dict, us_state

def print_daily(rows_dict):
    """ Prints the covid19 data for a given state.
    Parameters:
    values (death, ICU, hospitalized)
    """

    
    print(f"Daily data in decending order is for {us_state} ")
    #print(f"Date{'':6}Death{'':2} ICU{'':1}  Hosp'd{''} onVent ")
    pp('Date  Death   ICU   inHos  onVen')
    
    #pp.pprint(rows_dict)
    print('-'*30)
    for i in rows_dict:
        pp(f'{(str(i)[-4:]):2}:  {str(rows_dict[i][0]):6} {str(rows_dict[i][1]):6} {str(rows_dict[i][2]):6} {str(rows_dict[i][3]):3} ')

if __name__ == "__main__":
    daily_data = get_daily_data()
    rows_dict, us_state = data_by_state()
    print_daily(rows_dict)
